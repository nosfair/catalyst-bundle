### requirements
* [Symfony Messenger](https://symfony.com/doc/current/messenger.html) MessageBus

### usage
* define a message transport
  * [see Symfony Docs](https://symfony.com/doc/current/messenger.html#transports-async-queued-messages)
  * you can specify a dedicated transport for **Nosfair\CatalystBundle\Message\EventBusMessage**
* create an EventHandler for any event you want to listen to
```php
use Nosfair\CatalystBundle\Attribute\EventHandler;
use Nosfair\CatalystBundle\Contract\Example\DummySubjectInterface;
use Nosfair\CatalystBundle\Enum\EventMethod;

#[EventHandler(DummySubjectInterface::class, EventMethod::Created)]
class OnDummyCreated
{
    public function __invoke(DummySubjectInterface $subject)
    {
        // handle subject
    }
}
```