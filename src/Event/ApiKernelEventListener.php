<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Event;

use Nosfair\CatalystBundle\Contract\EventBusSubjectInterface;
use Nosfair\CatalystBundle\Enum\EventMethod;
use Nosfair\CatalystBundle\Service\EventBusService;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ApiKernelEventListener implements EventSubscriberInterface
{
    public function __construct(readonly private EventBusService $eventBusService)
    {
    }

    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::VIEW => [
                "handleKernvelEvent",
                31
            ]
        ];
    }

    public function handleKernvelEvent(ViewEvent $event): void
    {
        $subject = $event->getControllerResult();
        $requestMethod = $event->getRequest()->getMethod();

        if ($subject instanceof EventBusSubjectInterface && ($method = EventMethod::fromSymfonyRequestMethod($requestMethod)) !== null) {
            $this->eventBusService->submitEventSubject($subject, $method);
        }
    }
}
