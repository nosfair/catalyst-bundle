<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle;

use Nosfair\CatalystBundle\Attribute\EventHandler;
use Symfony\Component\DependencyInjection\ChildDefinition;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Loader\Configurator\ContainerConfigurator;
use Symfony\Component\HttpKernel\Bundle\AbstractBundle;

class NosfairCatalystBundle extends AbstractBundle
{
    public function loadExtension(array $config, ContainerConfigurator $container, ContainerBuilder $builder): void
    {
        $container->import("./Resources/config/services.yaml");
        $builder->registerAttributeForAutoconfiguration(EventHandler::class, static function (ChildDefinition $definition, EventHandler $attribute): void {
            $definition->addTag("kernel.event_listener", ["event" => $attribute->event]);
        });
    }
}
