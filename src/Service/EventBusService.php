<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Service;

use Nosfair\CatalystBundle\Contract\EventBusSubjectInterface;
use Nosfair\CatalystBundle\Enum\EventMethod;
use Nosfair\CatalystBundle\Message\EventBusMessage;
use Symfony\Component\Messenger\MessageBusInterface;

class EventBusService
{
    public function __construct(
        readonly protected MessageBusInterface $messageBus,
        readonly protected SerializerService $serializerService,
    ) {
    }

    public function submitEventSubject(EventBusSubjectInterface $subject, EventMethod $method): void
    {
        $contract = EventBusSubjectInterface::class;
        foreach (class_implements($subject) as $interface) {
            //if(preg_match('/^Nosfair\\\\CatalystBundle\\\\Contract\\\\\S+\\\\*$/', $interface)) {
            if (in_array(EventBusSubjectInterface::class, class_implements($interface))) {
                $contract = $interface;
                break;
            }
        }

        $serializedSubject = $this->serializerService->serializeSubject($subject, $contract);
        $nessage = new EventBusMessage($contract, $subject->getSubjectId(), $method->value, $serializedSubject);

        $this->messageBus->dispatch($nessage);
    }
}
