<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Service;

use Nosfair\CatalystBundle\Attribute\Contract;
use Symfony\Component\Serializer\SerializerInterface;

class SerializerService
{
    public const FORMAT = "json";

    public function __construct(
        readonly private SerializerInterface $serializer,
    ) {
    }

    /**
     * @template T
     * @param T|null $subject
     * @param class-string<T> $contract
     * @return string|null
     */
    public function serializeSubject(mixed $subject, string $contract): ?string
    {
        $serializedOriginalObject = $this->serializer->serialize($subject, self::FORMAT);
        $deserializedWithContract = $this->deserializeSubject($serializedOriginalObject, $contract);
        return $this->serializer->serialize($deserializedWithContract, self::FORMAT);
    }

    /**
     * @template T
     * @param string|null $subject
     * @param class-string<T> $contract
     * @return T|null
     */
    public function deserializeSubject(?string $subject, string $contract): mixed
    {
        return $this->serializer->deserialize($subject, Contract::fromContractClass($contract)->getDtoClass(), self::FORMAT);
    }
}
