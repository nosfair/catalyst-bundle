<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Enum;

use Symfony\Component\HttpFoundation\Request;

enum EventMethod: string
{
    case Created = "created";
    case Changed = "changed";
    case Deleted = "deleted";

    public static function fromSymfonyRequestMethod(string $requestMethod): ?self
    {
        return match ($requestMethod) {
            Request::METHOD_POST => self::Created,
            Request::METHOD_PUT, Request::METHOD_PATCH => self::Changed,
            Request::METHOD_DELETE => self::Deleted,
            default => null,
        };
    }
}
