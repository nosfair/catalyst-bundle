<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Message;

use Nosfair\CatalystBundle\Attribute\EventHandler;
use Nosfair\CatalystBundle\Enum\EventMethod;
use Nosfair\CatalystBundle\Service\SerializerService;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

final class EventBusHandler
{
    public function __construct(
        readonly private EventDispatcherInterface $eventDispatcher,
        readonly private SerializerService $serializerService,
    ) {
    }

    public function __invoke(EventBusMessage $message)
    {
        $subject = $this->serializerService->deserializeSubject($message->getSubject(), $message->getContract());
        $method = EventMethod::from($message->getMethod());
        $eventName = EventHandler::buildEventNameFromContractClassAndMethod($message->getContract(), $method);

        $this->eventDispatcher->dispatch($subject, $eventName);
    }
}
