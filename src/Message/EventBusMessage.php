<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Message;

final class EventBusMessage
{
    public function __construct(
        readonly private string $contract,
        readonly private string $subjectId,
        readonly private string $method,
        readonly private ?string $subject,
    ) {
    }

    public function getContract(): string
    {
        return $this->contract;
    }

    public function getSubjectId(): string
    {
        return $this->subjectId;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function getSubject(): ?string
    {
        return $this->subject;
    }
}
