<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Attribute;

use Nosfair\CatalystBundle\Enum\EventMethod;
use Symfony\Component\EventDispatcher\Attribute\AsEventListener;

#[\Attribute]
final class EventHandler extends AsEventListener
{
    public const EVENT_PREFIX = "nosfair_catalyst";

    public function __construct(string $contract, EventMethod $method)
    {
        parent::__construct(self::buildEventNameFromContractClassAndMethod($contract, $method));
    }

    public static function buildEventNameFromContractIdentifierAndMethod(string $contractIdentifier, EventMethod $method): string
    {
        return self::EVENT_PREFIX . ".$contractIdentifier.{$method->value}";
    }

    public static function buildEventNameFromContractClassAndMethod(string $contractClass, EventMethod $method): string
    {
        return self::buildEventNameFromContractIdentifierAndMethod(Contract::fromContractClass($contractClass)->getIdentifier(), $method);
    }
}
