<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Attribute;

#[\Attribute]
final class Contract
{
    public function __construct(
        readonly private string $identifier,
        readonly private string $dtoClass,
    ) {
    }

    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    public function getDtoClass(): string
    {
        return $this->dtoClass;
    }

    public static function fromContractClass(string $contractClass): self
    {
        $reflection = new \ReflectionClass($contractClass);
        $attributes = $reflection->getAttributes(self::class);
        $contractAttribute = array_pop($attributes);

        if ($contractAttribute === null) {
            throw new \Exception();
        }

        return $contractAttribute->newInstance();
    }
}
