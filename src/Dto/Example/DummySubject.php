<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Dto\Example;

use Nosfair\CatalystBundle\Contract\Example\DummySubjectInterface;
use Nosfair\CatalystBundle\Dto\EventBusSubject;

class DummySubject extends EventBusSubject implements DummySubjectInterface
{
    private string $dummyField;

    public function getDummyField(): string
    {
        return $this->dummyField;
    }

    public function setDummyField(string $dummyField): DummySubject
    {
        $this->dummyField = $dummyField;
        return $this;
    }
}
