<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Dto;

use Nosfair\CatalystBundle\Contract\EventBusSubjectInterface;

class EventBusSubject implements EventBusSubjectInterface
{
    private string $subjectId;

    public function getSubjectId(): string
    {
        return $this->subjectId;
    }

    public function setSubjectId(string $subjectId): EventBusSubject
    {
        $this->subjectId = $subjectId;
        return $this;
    }
}
