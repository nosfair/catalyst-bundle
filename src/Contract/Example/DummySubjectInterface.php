<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Contract\Example;

use Nosfair\CatalystBundle\Attribute\Contract;
use Nosfair\CatalystBundle\Contract\EventBusSubjectInterface;
use Nosfair\CatalystBundle\Dto\Example\DummySubject;

#[Contract(identifier: "example_dummy", dtoClass: DummySubject::class)]
interface DummySubjectInterface extends EventBusSubjectInterface
{
    public function getDummyField(): string;
}
