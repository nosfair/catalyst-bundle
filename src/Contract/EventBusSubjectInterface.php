<?php

declare(strict_types=1);

namespace Nosfair\CatalystBundle\Contract;

use Nosfair\CatalystBundle\Attribute\Contract;
use Nosfair\CatalystBundle\Dto\EventBusSubject;

#[Contract(identifier: "subject", dtoClass: EventBusSubject::class)]
interface EventBusSubjectInterface
{
    public function getSubjectId(): string;
}
